package com.division.poc.representation.controller;

import com.division.poc.representation.dto.DAlertData;
import com.division.poc.representation.service.AlertApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlertController {

    @Autowired
    private AlertApplicationService alertApplicationService;

    @PostMapping(value = "/alert", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAlert(@RequestBody DAlertData payload) {
        return ResponseEntity.ok(alertApplicationService.createAlert(payload));
    }

}
