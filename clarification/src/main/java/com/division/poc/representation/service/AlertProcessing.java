package com.division.poc.representation.service;

import com.division.poc.model.domainmodel.MAlert;

public interface AlertProcessing {
    void beforeProcessing();

    void afterProcessing(MAlert alert);
}
