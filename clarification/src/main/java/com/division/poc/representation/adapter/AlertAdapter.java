package com.division.poc.representation.adapter;

import com.division.poc.model.domainmodel.MAlert;
import com.division.poc.model.domainmodel.MAlertData;
import com.division.poc.representation.dto.DAlert;
import com.division.poc.representation.dto.DAlertData;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AlertAdapter {

    public MAlertData fromDtoToModel(DAlertData alert) {

        MAlertData mAlert = new MAlertData();
        mAlert.setTypeId(alert.getAlertType());
        mAlert.setPriority(alert.getPriority());

        return mAlert;
    }

    public DAlert fromModelToDto(MAlert alert, Map extension) {

        DAlert dAlert = new DAlert();
        dAlert.setId(alert.getId());

        dAlert.setData(DAlertData.builder()
                .alertType(alert.getAlertType())
                .priority(alert.getPriority())
                .extension(extension)
                .build()
        );

        return dAlert;
    }
}
