package com.division.poc.representation.service;

import com.division.poc.model.modelextension.AlertExtension;

import java.util.Map;

public interface AlertExtensionService {
    AlertExtension createAlertExtension(Map extension);

    Map formalizeExtension(AlertExtension extension);
}
