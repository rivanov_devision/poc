package com.division.poc.representation.service;

import com.division.poc.model.domainmodel.MAlert;
import com.division.poc.model.service.DomainAlertService;
import com.division.poc.representation.adapter.AlertAdapter;
import com.division.poc.representation.dto.DAlert;
import com.division.poc.representation.dto.DAlertData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Collections;

@Service
public class AlertApplicationService<T> {

    //Operate on model level
    @Autowired
    private DomainAlertService service;

    //Custom implementation provided
    @Autowired(required = false)
    private AlertExtensionService extensionService;

    //Injecting custom processing
    @Autowired(required = false)
    private AlertProcessing alertProcessing;

    @Autowired
    private AlertAdapter adapter;

    public DAlert createAlert(DAlertData data) {

        MAlert alert;

        if (!ObjectUtils.isEmpty(alertProcessing)) {
            alertProcessing.beforeProcessing();
        }

        alert = service.create(adapter.fromDtoToModel(data),
                !ObjectUtils.isEmpty(extensionService) &&
                !ObjectUtils.isEmpty(data.getExtension()) ?
                extensionService.createAlertExtension(data.getExtension()) :
                null);

        if (!ObjectUtils.isEmpty(alertProcessing)) {
            alertProcessing.afterProcessing(alert);
        }

        return adapter.fromModelToDto(alert, !ObjectUtils.isEmpty(extensionService) ?
                extensionService.formalizeExtension(alert.getAlertExtension()) : Collections.EMPTY_MAP);
    }

}
