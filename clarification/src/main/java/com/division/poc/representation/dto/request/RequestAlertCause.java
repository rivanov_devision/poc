package com.division.poc.representation.dto.request;

import com.division.poc.model.enumeration.Priority;
import lombok.Data;

import java.util.List;

@Data
public class RequestAlertCause {
    private Priority priority;
    private String reason;
    private Long scenarioName;
    private Long accModule;
    private String suppressionDefinition;
    private List<Long> coverage;


}
