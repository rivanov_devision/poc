package com.division.poc.representation.dto;

import lombok.Data;

@Data
public class DRuleInfo {
    private String nodeId;
    private String version;
}
