package com.division.poc.representation.dto;


import lombok.Data;

@Data
public class DAlertCause {
    private Long id;
    private DAlertCauseData data;
}
