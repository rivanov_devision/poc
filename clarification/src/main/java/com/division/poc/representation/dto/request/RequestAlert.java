package com.division.poc.representation.dto.request;

import com.division.poc.model.enumeration.Priority;
import lombok.Data;

@Data
public class RequestAlert {
    private Priority priority;
    private Long alertType;

}
