package com.division.poc.representation.dto;

import lombok.Data;

@Data
public class DAlert {
    private Long id;
    private DAlertData data;
}
