package com.division.poc.representation.dto;

import com.division.poc.model.enumeration.Priority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Map;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DAlertData {

    private Priority priority;
    private Long alertType;
    Map<String, Object> extension;

}
