package com.division.poc.representation.dto;

import com.division.poc.model.enumeration.Priority;
import lombok.*;

import java.util.List;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DAlertCauseData {
    private Priority priority;
    private String reason;
    private String scenarioName;
    private String accModule;
    private String suppressionDefinition;
    private List<String> coverage;


}
