package com.division.poc.model;

import com.division.poc.model.embedded.DateAndUser;
import com.division.poc.model.embedded.ParameterizedMessage;
import com.division.poc.model.enumeration.ClarificationState;
import com.division.poc.model.enumeration.Priority;
import com.division.poc.model.extension.DecisionEnum;
import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@Builder()
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity(name = "clarification")
@Table(name = "clarification",schema = "poc_clarification")
public final class Clarification {

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    @EqualsAndHashCode.Include
    private Long id;

    @Version
    @Column(name = "objectVersion")
    private Integer objectVersion;

    @Column(name = "remark")
    private String remark;

    @Column(name = "clarificationState", nullable = false)
    @NonNull
    private ClarificationState clarificationState;

    @ManyToOne
    @JoinColumn(name = "decision", referencedColumnName = "id")
    private DecisionEnum decision;

    @Column(name = "priority", nullable = false)
    @NonNull
    private Priority priority;

    @Embedded
    @NonNull
    private ParameterizedMessage message;

    @Embedded
    //@NonNull
    @AttributeOverrides({
            @AttributeOverride(name = "userId", column = @Column(name = "created_User")),
            @AttributeOverride(name = "date", column = @Column(name = "created_Date"))
    })
    private DateAndUser created;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "userId", column = @Column(name = "modified_User")),
            @AttributeOverride(name = "date", column = @Column(name = "modified_Date"))
    })
    private DateAndUser lastModified;

    @PrePersist
    protected void onPersist() {
        if (this.created == null) {
            this.created = DateAndUser.of(0L, ZonedDateTime.now().toInstant());
        }
    }

    @PreUpdate
    protected void onUpdate() {
        if (this.lastModified == null) {
            this.lastModified = new DateAndUser();
            this.lastModified.setUserId(0L);
        }
        this.lastModified.setDate(ZonedDateTime.now().toInstant());
    }

}
