package com.division.poc.model.extension;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString

@Entity
@Table(name = "decision_enum",schema = "poc_clarification")
public final class DecisionEnum {

    private @Id
    Long id;

    @Column
    private String name;

}
