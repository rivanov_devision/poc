package com.division.poc.model;

import com.division.poc.model.embedded.DateAndUser;
import com.division.poc.model.enumeration.Priority;
import com.division.poc.model.extension.AlertType;
import com.division.poc.model.modelextension.AlertExtension;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Setter

@Entity(name = "alert")
@Table(name = "alert", schema = "poc_clarification")
public final class Alert {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "priority")
    @NonNull
    private Priority priority;

    @ManyToOne
    @JoinColumn(name = "alertType")
    private AlertType alertType;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "userId", column = @Column(name = "created_User")),
            @AttributeOverride(name = "date", column = @Column(name = "created_Date"))
    })
    private DateAndUser createdDate;


    @OneToOne(optional = true, cascade = CascadeType.ALL, mappedBy = "alert", targetEntity = AlertExtension.class)
    @NotFound(action = NotFoundAction.IGNORE)
    private AlertExtension alertExtension;

}
