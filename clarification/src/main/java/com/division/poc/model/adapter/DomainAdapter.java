package com.division.poc.model.adapter;

import com.division.poc.model.Alert;
import com.division.poc.model.domainmodel.MAlert;
import org.springframework.stereotype.Component;

@Component
public class DomainAdapter {

    public MAlert fromEntityToModel(Alert alert) {
        MAlert mAlert = new MAlert();
        mAlert.setId(alert.getId());
        mAlert.setAlertType(alert.getAlertType().getId());
        mAlert.setAlertExtension(alert.getAlertExtension());
        mAlert.setPriority(alert.getPriority());

        return mAlert;
    }
}
