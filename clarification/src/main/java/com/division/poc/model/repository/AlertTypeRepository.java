package com.division.poc.model.repository;

import com.division.poc.model.extension.AlertType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertTypeRepository extends JpaRepository<AlertType, Long> {
}
