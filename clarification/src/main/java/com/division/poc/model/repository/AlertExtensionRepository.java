package com.division.poc.model.repository;

import com.division.poc.model.modelextension.AlertExtension;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlertExtensionRepository extends JpaRepository<AlertExtension, Long> {
}
