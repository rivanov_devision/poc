package com.division.poc.model.embedded;

import lombok.*;

import javax.persistence.Embeddable;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString

@Embeddable
public class SuppressionDefinition {
    private String keyValue;
}
