package com.division.poc.model.embedded;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
@Getter
@EqualsAndHashCode
@ToString

@Embeddable
public class ParameterizedMessage {

    @Column(name="message", nullable = false)
    private String message;
}
