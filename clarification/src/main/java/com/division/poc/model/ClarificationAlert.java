package com.division.poc.model;


import javax.persistence.*;

@Entity
@Table(name = "clarification_alert",schema = "poc_clarification")
public class ClarificationAlert {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "alert_id", nullable = false)
    private Alert alert;

    @ManyToOne
    @JoinColumn(name = "clarification_id", nullable = false)
    private Clarification clarification;
}
