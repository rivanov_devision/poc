package com.division.poc.model;

import com.division.poc.model.embedded.ParameterizedMessage;
import com.division.poc.model.embedded.RuleInfo;
import com.division.poc.model.embedded.SuppressionDefinition;
import com.division.poc.model.enumeration.Priority;
import com.division.poc.model.extension.AccsModule;
import com.division.poc.model.extension.Coverage;
import com.division.poc.model.extension.ScenarioName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Set;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor

@Entity(name = "alertCause")
@Table(name = "alert_cause",schema = "poc_clarification")
public final class AlertCause {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "Cla02_Alert")
    @Setter
    @JsonIgnore
    private Alert alert;

    @Column(name = "priority")
    @NonNull
    private Priority priority;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "message", column = @Column(name = "reason"))
    })
    @NonNull
    private ParameterizedMessage reason;

    @ManyToOne()
    @JoinColumn(name = "scenarioName")
    private ScenarioName scenarioName;

    @ManyToOne()
    @JoinColumn(name = "modul")
    @Nullable
    private AccsModule modul;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "keyValue", column = @Column(name = "suppressionDefinition_keyvalue"))})
    @Nullable
    private SuppressionDefinition suppressionDefinition;


    @ManyToMany()
    @JoinTable(name = "Cla06_AlertCauseCoverage",
            joinColumns = {@JoinColumn(name = "Cla01_AlertCause")},
            inverseJoinColumns = {@JoinColumn(name = "Coverage")})
    private Set<Coverage> coverage;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "nodeId", column = @Column(name = "ruleInfo_nodeId")), @AttributeOverride(name = "version", column = @Column(name = "ruleInfo_version"))})
    @Nullable
    private RuleInfo ruleInfo;
}
