package com.division.poc.model.embedded;

import lombok.*;

import javax.persistence.Embeddable;
import java.time.Instant;


@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@Setter
@ToString
@EqualsAndHashCode

@Embeddable
public final class DateAndUser {

    private Long userId;

    private Instant date;

}
