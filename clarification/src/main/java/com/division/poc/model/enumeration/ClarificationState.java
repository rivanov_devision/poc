package com.division.poc.model.enumeration;

public enum ClarificationState {
    NEW,
    IN_PROGRESS,
    CLOSED,
    EXTERN,
    IRRELEVANT,
    ARCHIVED,
}
