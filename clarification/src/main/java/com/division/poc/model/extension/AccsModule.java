package com.division.poc.model.extension;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
@Getter
@NoArgsConstructor
@ToString

@Entity(name = "modul")
@Table(name = "module",schema = "poc_clarification")
public class AccsModule {

    @Id
    private String module;
}
