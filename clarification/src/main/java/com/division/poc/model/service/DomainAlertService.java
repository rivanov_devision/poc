package com.division.poc.model.service;

import com.division.poc.model.Alert;
import com.division.poc.model.adapter.DomainAdapter;
import com.division.poc.model.domainmodel.MAlert;
import com.division.poc.model.domainmodel.MAlertData;
import com.division.poc.model.embedded.DateAndUser;
import com.division.poc.model.enumeration.Priority;
import com.division.poc.model.extension.AlertType;
import com.division.poc.model.modelextension.AlertExtension;
import com.division.poc.model.repository.AlertExtensionRepository;
import com.division.poc.model.repository.AlertRepository;
import com.division.poc.model.repository.AlertTypeRepository;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class DomainAlertService {

    private final AlertRepository alertRepository;

    private final AlertTypeRepository alertTypeRepository;

    private final AlertExtensionRepository alertExtensionRepository;

    private final DomainAdapter domainAdapter;

    private DomainAlertService(AlertRepository alertRepository,
                               AlertTypeRepository alertTypeRepository,
                               AlertExtensionRepository alertExtensionRepository,
                               DomainAdapter adapter) {
        this.alertRepository = alertRepository;
        this.alertTypeRepository = alertTypeRepository;
        this.domainAdapter = adapter;
        this.alertExtensionRepository = alertExtensionRepository;
    }

    public MAlert create(MAlertData mAlertData, AlertExtension alertExtension) {

        AlertType alertType = alertTypeRepository.getOne(mAlertData.getTypeId());

        Alert alert = new Alert();
        alert.setAlertType(alertType);
        alert.setCreatedDate(DateAndUser.of(1l, ZonedDateTime.now().toInstant()));
        alert.setPriority(mAlertData.getPriority());

        if (alertExtension != null) {
            alert.setAlertExtension(alertExtension);
            alertExtension.setAlert(alert);
        }

        return domainAdapter.fromEntityToModel(alertRepository.save(alert));

    }

    private Object getAlertExtension(Long id) {
        return alertExtensionRepository.getOne(id);
    }
}
