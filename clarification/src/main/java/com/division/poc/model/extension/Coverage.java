package com.division.poc.model.extension;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString

@Entity(name="coverage")
@Table(name="coverage",schema = "poc_clarification")
public final class Coverage {

    @Id
    private Long id;

    @Column
    private String name;
}
