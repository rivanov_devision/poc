package com.division.poc.model.extension;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString

@Entity(name = "alertType")
@Table(name = "alert_type",schema = "poc_clarification")
public final class AlertType {
    @Id
    private Long id;

    @Column
    private String name;
}
