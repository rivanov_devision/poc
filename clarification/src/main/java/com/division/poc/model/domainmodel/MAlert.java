package com.division.poc.model.domainmodel;

import com.division.poc.model.enumeration.Priority;
import com.division.poc.model.modelextension.AlertExtension;
import lombok.Data;

@Data
public class MAlert {
    private Long id;

    private Priority priority;

    private Long alertType;

    private AlertExtension alertExtension;
}
