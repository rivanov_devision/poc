package com.division.poc.model.repository;

import com.division.poc.model.AlertCause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertCauseRepository extends JpaRepository<AlertCause, Long> {
}
