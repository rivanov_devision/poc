package com.division.poc.model.repository;

import com.division.poc.model.Clarification;
import com.division.poc.model.enumeration.Priority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClarificationRepository extends JpaRepository<Clarification, Long> {
    List<Clarification> findClarificationsByPriority(Priority priority);
}
