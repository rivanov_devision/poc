package com.division.poc.model.enumeration;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW,
    LOWEST,
    HIGHEST
}
