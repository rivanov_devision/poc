package com.division.poc.model.extension;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString

@Entity(name = "scenarioName")
@Table(name = "Scenario_name",schema = "poc_clarification")
public class ScenarioName {
    @Id
    private String id;

    @Column
    private String name;
}
