package com.division.poc.model.embedded;

import lombok.*;

import javax.persistence.Embeddable;

@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@ToString
@EqualsAndHashCode

@Embeddable
public class RuleInfo {

    private String nodeId;
    private String version;
}
