package com.division.poc.model.modelextension;

import com.division.poc.model.Alert;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "alert_extension", schema = "poc_clarification")
@Inheritance(strategy = InheritanceType.JOINED)
public class AlertExtension {

    @Id
    private Long id;

    @MapsId
    @OneToOne(optional = false)
    @JoinColumn(name = "id", referencedColumnName = "id")
    @JsonIgnore
    private Alert alert;

}
