package com.division.poc.model.domainmodel;


import com.division.poc.model.enumeration.Priority;
import lombok.Data;

@Data
public class MAlertData {
    Priority priority;
    Long typeId;
}
