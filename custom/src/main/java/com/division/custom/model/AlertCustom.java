package com.division.custom.model;

import com.division.poc.model.modelextension.AlertExtension;
import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@Builder

@Entity
@Table(name = "custom_alert", schema = "poc_custom")
public class AlertCustom extends AlertExtension {

    @Column(name = "custom_message")
    @NonNull
    private String customMessage;

}
