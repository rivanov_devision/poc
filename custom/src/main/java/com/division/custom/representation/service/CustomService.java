package com.division.custom.representation.service;

import com.division.custom.model.AlertCustom;
import com.division.poc.model.modelextension.AlertExtension;
import com.division.poc.representation.service.AlertExtensionService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class CustomService implements AlertExtensionService {


    @Override
    public AlertExtension createAlertExtension(Map extension) {
        if (!ObjectUtils.isEmpty(extension)) {
            return new AlertCustom((String) extension.get("message"));
        } else {
            throw new RuntimeException("Missing message");
        }

    }

    @Override
    public Map formalizeExtension(AlertExtension extension) {
        if (!ObjectUtils.isEmpty(extension)) {
            Map<String, Object> extend = new HashMap<>();
            extend.put("id", ((AlertCustom) extension).getId());
            extend.put("message", ((AlertCustom) extension).getCustomMessage());
            return extend;
        } else {
            return Collections.emptyMap();
        }
    }
}
